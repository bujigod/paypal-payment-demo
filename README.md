# paypal支付接口demo

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明


  主要流程 
1.注册paypal商户账号
2.登录开发者平台或者创建开发者账号 https://developer.paypal.com”
3.创建应用,可获取必要参数,clientId和clientSecret
4.沙盒中创建消费账户和商家账户(别用默认的,可能会比较慢,重新添加选中国的)
5.启动本项目,访问localhost:8088,点击paypal,登录沙盒中的消费账户付款,支付或取消跳转对应重定向地址
6.用沙盒中账户登录https://www.sandbox.paypal.com/c2/signin查看消费记录
7.退款参考main方法

本项目中Client中参数为我自己沙盒中的参数,可以直接用,但是后期可能我回删除,最好自己申请
沙盒中商户号  sb-gqccf1625086@business.example.com  密码D/h5z]p*
消费号:sb-qyiyd6226812@personal.example.com 密码 3+Bxe/<8
注:本项目参照paypal开发者网站中的api模块中sdk快速开发以及https://blog.csdn.net/qq_34579313/article/details/84848122


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
